A Chrome Extension which analyzes live betting offers on sports.williamhill.com and picks interesting matches based on various parameters. The parameters depend on the sport: set number for tennis, match minute for football, betting coefficient for both.

Some of the features are:

*   match tracking: opens interesting matches, closes matches that are no longer interesting and remembers not to open them again
*   notifications: audio and visual cues when a match is opened
*   reminders: audio and visual cues when a certain user defined condition is met e.g. a certain amount of games have been played in a set of a tennis match, a certain amount of time has passed in a football match
*   changes fractional coefficients to decimal representation and various other improvements

The extension is written in TypeScript with extensive use of jQuery and Chrome API. The extension also uses HTML manipulation for some of its features.
To install the extension, open the app folder in Chrome as described here: https://developer.chrome.com/extensions/getstarted#unpacked