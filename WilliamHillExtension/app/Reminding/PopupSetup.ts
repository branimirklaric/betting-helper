﻿//klikni na popup
//popup traži od content scripta objekt za stvaranje obavjesti
//kod stvaranje obavjesti se također javlja content scriptu da kontrolira nešto (npr. gemove)
//kada se to dogodi, content script javlja backgroundu, koji odašilje obavjest

namespace Reminding {
    export class PopupSetup {
        constructor(private reminderFormSetupFactory: ReminderFormSetupFactory) {
        }

        setUpPopup() {
            this.getActiveMatchTab();
        }

        private getActiveMatchTab() {
            var activeMatchTabQuery = {
                active: true,
                url: "http://sports.williamhill.com/bet/en-gb/betting/e/*"
            };
            chrome.tabs.query(
                activeMatchTabQuery, this.getReminderFormSetup.bind(this));
        }

        private getReminderFormSetup(tabs: chrome.tabs.Tab[]) {
            if (tabs.length == 0) {
                $("body").append("<p>You cannot place reminders here.</p>");
                return;
            }
            var matchTabId = tabs[0].id;
            var message = { getReminderModel: true, matchTabId: matchTabId };
            chrome.tabs.sendMessage(
                matchTabId, message, this.useReminderFormModel.bind(this));
        }

        private useReminderFormModel(message) {
            if (message.sportName === undefined) {
                $("body").append("<p>Wait for the page to load and try again.</p>")
            } else {
                var reminderFormSetup =
                    this.reminderFormSetupFactory.createReminderFormSetup(
                        message.sportName, message.reminderFormModel);
                reminderFormSetup.setUpReminderForm();
            }
        }
    }
}