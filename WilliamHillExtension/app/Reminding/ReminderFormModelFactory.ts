﻿namespace Reminding {
    export class ReminderFormModelFactory {
        createReminderFormModel(sportName: string, matchTabId: number) {
            var remindingModule = window["Reminding"];
            var reminderFormModelClassName = sportName + "ReminderFormModel";
            var reminderFormModel = Object.create(
                remindingModule[reminderFormModelClassName].prototype);
            reminderFormModel.constructor.call(reminderFormModel, matchTabId);
            return reminderFormModel;
        }
    }
}