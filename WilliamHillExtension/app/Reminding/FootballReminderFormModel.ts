﻿namespace Reminding {
    export class FootballReminderFormModel {
        time: string;
        matchTabId: number;

        constructor(matchTabId: number) {
            this.time = $("#scoreboard_frame").contents().find("#time").text();
            this.matchTabId = matchTabId;
        }
    }
}