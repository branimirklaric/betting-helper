﻿namespace Reminding {
    export class FootballReminderFormSetup implements ReminderFormSetup {
        constructor(private formSetupModel: FootballReminderFormModel) {
        }

        //puts a football match time reminder form in the popup
        setUpReminderForm() {
            $("body").append("<p>Current match time: " + this.formSetupModel.time + "</p>");

            var timeReminderTable = "<table>";
            timeReminderTable += this.setReminderFormForTime();
            timeReminderTable += "</table>";

            $("body").append(timeReminderTable);

            this.setFootballOnClickReminders();
        }

        //assembles the reminder form for match time
        private setReminderFormForTime() {
            var timeReminderRow = "<tr><td>";
            var selectTimeTag = "<select id='timeSelect'>";
            var minutes = parseInt(this.formSetupModel.time.split(":")[0]);
            var startOptionMinutes = 0;
            while (startOptionMinutes <= minutes) {
                startOptionMinutes += 5;
            }
            for (var i = startOptionMinutes; i <= 95; i += 5) {
                selectTimeTag += "<option value=" + i + ">" + i + ":00</option>";
            }
            selectTimeTag += "</select>";
            timeReminderRow += selectTimeTag + "</td>";

            timeReminderRow +=
            "<td id='timeRemind'><u>Put a reminder on time.</u></td></tr>";

            return timeReminderRow;
        }

        //sets up the onclick event that sets up reminders
        private setFootballOnClickReminders() {
            $("#timeRemind").click(() => {
                this.setTimeReminder($("#timeSelect").val());
            });
        }

        //sends a message to match tab to set up a reminder
        private setTimeReminder(minute) {
            var reminderMessageObject = {
                sportToTrack: "Football",
                reminderTrackerModel: new FootballReminderTrackerModel(parseInt(minute))
            };
            chrome.tabs.sendMessage(this.formSetupModel.matchTabId, reminderMessageObject);
            $("body").append("<p>A reminder for time " + minute + ":00 is set.</p>");
        }
    }
}