﻿namespace Reminding {
    export class TennisReminderTrackerModel {
        constructor(public playerLetter: string, public games: number) {
        }
    }
}