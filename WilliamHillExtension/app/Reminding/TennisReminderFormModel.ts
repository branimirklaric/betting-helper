﻿namespace Reminding {
    export class TennisReminderFormModel {
        gamesA: number;
        gamesB: number;
        matchTabId: number;

        constructor(matchTabId: number) {
            this.matchTabId = matchTabId;
            var scoreboardContents = $("#scoreboard_frame").contents();
            this.gamesA = parseInt(scoreboardContents.find("#games_A").text());
            this.gamesB = parseInt(scoreboardContents.find("#games_B").text());
        }
    }
}