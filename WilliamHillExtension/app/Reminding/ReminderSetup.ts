﻿namespace Reminding {
    export class ReminderSetup {
        constructor(private reminderTrackerFactory: ReminderTrackerFactory) {
        }

        useReminderTracker(message) {
            if (message.sportToTrack === undefined) {
                return;
            }
            var reminderTracker = this.reminderTrackerFactory.createReminderTracker(
                message.sportToTrack, message.reminderTrackerModel);
            reminderTracker.trackReminder();
        }
    }
}