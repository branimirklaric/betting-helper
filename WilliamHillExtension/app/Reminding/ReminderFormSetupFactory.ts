﻿namespace Reminding {
    export class ReminderFormSetupFactory {
        createReminderFormSetup(
                sportName: string, reminderFormModel): ReminderFormSetup {
            var remindingModule = window["Reminding"];
            var reminderFormSetupClassName = sportName + "ReminderFormSetup";
            var reminderFormSetup: ReminderFormSetup = Object.create(
                remindingModule[reminderFormSetupClassName].prototype);
            reminderFormSetup.constructor.call(reminderFormSetup, reminderFormModel);
            return reminderFormSetup;
        }
    }
}