﻿namespace Reminding {
    export class ReminderTrackerFactory {
        constructor(private titleChanger: TitleChanger) {
        }

        createReminderTracker(
                sportName: string, reminderTrackerModel): ReminderTracker {
            var remindingModule = window["Reminding"];
            var reminderTrackerClassName = sportName + "ReminderTracker";
            var reminderFormSetup: ReminderTracker = Object.create(
                remindingModule[reminderTrackerClassName].prototype);
            reminderFormSetup.constructor.call(
                reminderFormSetup, reminderTrackerModel, this.titleChanger);
            return reminderFormSetup;
        }
    }
}