﻿namespace Reminding {
    export class TitleChanger {
        //changes the title of tabs that have activated a reminder
        changeTitle(titlePrefix: string) {
            var d = new Date();
            var title = $("title").text();
            $("title").text(d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds()
                + " - " + titlePrefix + " - " + title);
        }
    }
}