﻿namespace Reminding {
    export class TennisReminderFormSetup implements ReminderFormSetup {
        constructor(private formSetupModel: TennisReminderFormModel) {
        }

        //puts a tennis games reminder form in the popup
        setUpReminderForm() {
            var popupHeader = "<p>Games A: " + this.formSetupModel.gamesA + "</p>"
                + "<p>Games B: " + this.formSetupModel.gamesB + "</p>";
            $("body").append(popupHeader);

            var gamesReminderTable = "<table>";
            gamesReminderTable += this.setReminderFormForGames("A");
            gamesReminderTable += this.setReminderFormForGames("B");
            gamesReminderTable += "</table>";

            $("body").append(gamesReminderTable);

            this.setTennisOnClickReminders("A");
            this.setTennisOnClickReminders("B");
        }

        //assembles the reminder form for player games
        private setReminderFormForGames(playerLetter: string) {
            var playerGames = <number> this.formSetupModel["games" + playerLetter];
            if (playerGames >= 6) {
                return;
            }
            var gamesReminderRow = "<tr><td>";
            var selectGamesTag = "<select id='games" + playerLetter + "Select'>";
            for (var i = playerGames + 1; i <= 6; ++i) {
                selectGamesTag += "<option value=" + i + ">" + i + "</option>";
            }
            selectGamesTag += "</select>";
            gamesReminderRow += selectGamesTag + "</td>";

            gamesReminderRow += "<td id='games" + playerLetter
            + "Remind'><u>Put a reminder on Games " + playerLetter + ".</u></td></tr>";

            return gamesReminderRow;
        }

        //sets up the onclick event that sets up reminders
        private setTennisOnClickReminders(playerLetter: string) {
            $("#games" + playerLetter + "Remind").click(() => {
                var gamesToRemind = $("#games" + playerLetter + "Select").val();
                this.setGamesReminder(playerLetter, parseInt(gamesToRemind));
            });
        }

        //sends a message to match tab to set up a reminder
        private setGamesReminder(playerLetter: string, games: number) {
            var reminderMessageObject = {
                sportToTrack: "Tennis",
                reminderTrackerModel: new TennisReminderTrackerModel(playerLetter, games)
            };
            chrome.tabs.sendMessage(this.formSetupModel.matchTabId, reminderMessageObject);
            $("body").append("<p>A reminder for games " + playerLetter + " "
                + games + " is set.</p>");
        }
    }
}