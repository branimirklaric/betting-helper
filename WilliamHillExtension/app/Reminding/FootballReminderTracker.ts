﻿namespace Reminding {
    export class FootballReminderTracker implements ReminderTracker {
        constructor(
            private reminderTrackerModel: FootballReminderTrackerModel,
            private titleChanger: TitleChanger) {
        }

        //periodically checks football match time to see if it should activate a reminder
        trackReminder() {
            var currentMinute =
                $("#scoreboard_frame").contents().find("#time").text().split(":")[0];
            if (parseInt(currentMinute) === this.reminderTrackerModel.minute) {
                console.log("sending a reminder");
                this.titleChanger.changeTitle(currentMinute + ":00");
                chrome.runtime.sendMessage({ activateReminder: true });
                return;
            }
            var trackReminderHandler = this.trackReminder.bind(this);
            setTimeout(trackReminderHandler, 10000);
        }
    }
}