﻿namespace Reminding {
    export interface ReminderFormSetup {
        setUpReminderForm(): void;
    }
}