﻿namespace Reminding {
    export interface ReminderTracker {
        trackReminder(): void;
    }
}