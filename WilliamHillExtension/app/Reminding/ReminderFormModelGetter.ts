﻿namespace Reminding {
    export class ReminderFormModelGetter {
        constructor(private reminderFormModelFactory: ReminderFormModelFactory) {
        }

        getReminderFormModel(message, sender: chrome.runtime.MessageSender,
            sendResponse: Function) {
            if (message.getReminderModel === undefined) {
                return;
            }
            var sportName = $("#breadcrumb").children()[1].innerText.trim();

            var reminderFormModel = this.reminderFormModelFactory
                .createReminderFormModel(sportName, message.matchTabId);
            sendResponse({ sportName: sportName, reminderFormModel: reminderFormModel });
        }
    }
}