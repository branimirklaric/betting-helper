﻿namespace Reminding{
    export class TennisReminderTracker implements ReminderTracker {
        constructor(
            private reminderTrackerModel: TennisReminderTrackerModel,
            private titleChanger: TitleChanger) {
        }

        //periodically checks player games to see if it should activate a reminder
        trackReminder() {
            var games = $("#scoreboard_frame").contents()
                .find("#games_" + this.reminderTrackerModel.playerLetter).text();
            if (parseInt(games) === this.reminderTrackerModel.games) {
                this.titleChanger.changeTitle(this.reminderTrackerModel.playerLetter
                    + " " + this.reminderTrackerModel.games);
                chrome.runtime.sendMessage({ activateReminder: true });
                return;
            }
            var trackReminderHandler = this.trackReminder.bind(this);
            setTimeout(trackReminderHandler, 10000);
        }
    }
}