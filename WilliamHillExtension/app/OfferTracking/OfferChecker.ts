﻿namespace OfferTracking {
	export class OfferChecker {
		private checkSportOfferCallback = this.checkSportOffer.bind(this);
        private observers: OfferTrackerObserver[] = [];

		constructor(
			    private priceFormatChecker: PriceFormatChecker,
                private offerAnalyzerFactory: OfferAnalyzerFactory) {
		}

        checkOffer() {
            $(".marketHolderCollapsed").find("thead").click();
            var sportCodes = EnumEx.getValues(SportCode);
            sportCodes.forEach(this.checkSportOfferCallback);
		}

		private checkSportOffer(sportCode: number) {
            var pairRows = $("#ip_sport_" + sportCode).find(".rowLive, .rowOdd");
			var fractionalPrices = this.priceFormatChecker.checkPriceFormats(pairRows);
            if (!fractionalPrices) {
				throw "reload";
			}

			var sportName = SportCode[sportCode];
			var offerAnalyzer = this.offerAnalyzerFactory.createOfferAnalyzer(sportName);
			var interestingMatchesUrls = offerAnalyzer.findInterestingMatches(pairRows);
			this.notify(interestingMatchesUrls);
		}

		addObserver(observerToAdd: OfferTrackerObserver) {
			this.observers.push(observerToAdd);
		}

		removeObserver(observerToRemove: OfferTrackerObserver) {
			var observerToRemoveIndex = this.observers.indexOf(observerToRemove);
			this.observers.splice(observerToRemoveIndex, 1);
		}

		private notify(matchUrls: string[]) {
			this.observers.forEach((observer: OfferTrackerObserver) => {
				observer.recieveMatchUrls(matchUrls);
			}); 
		}
	}

	export interface OfferTrackerObserver {
		recieveMatchUrls(matchUrls: string[]);
	}

	enum SportCode {
		Football = 9,
		Tennis = 24
    }
}

