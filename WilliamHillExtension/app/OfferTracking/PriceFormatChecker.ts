﻿namespace OfferTracking {
    export class PriceFormatChecker {
        private haveFractionalPrices = false;

		checkPriceFormats(pairRows: JQuery) {
            this.haveFractionalPrices = false;
            pairRows.each(this.checkPriceFormat.bind(this));
			return this.haveFractionalPrices;
		}

		private checkPriceFormat(index: number, elem: Element) {
			var priceContainers =
                $(elem).find(".eventpriceholder-left").find(".eventprice");
            for (var i = 0; i < priceContainers.length; ++i) {
                if (priceContainers.get(i).innerText.trim().indexOf("/") > -1) {
                    this.haveFractionalPrices = true;
					return false;
                }
			}
		}
	}
}