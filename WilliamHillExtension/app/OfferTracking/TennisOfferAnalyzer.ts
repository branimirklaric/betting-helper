﻿namespace OfferTracking {
	export class TennisOfferAnalyzer extends OfferAnalyzer {
		protected hasInterestingSpecifics(pairRow: Element) {
			var setNumber = parseInt($(pairRow).children()[0].innerText);
			return setNumber === 2;
		}
	}
}