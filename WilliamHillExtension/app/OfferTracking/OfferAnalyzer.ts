﻿namespace OfferTracking {
    //abstract class
    export class OfferAnalyzer {
        //template method
        findInterestingMatches(pairRows: JQuery) {
            var interestingMatchesUrls: Array<string> = [];
            pairRows.each((index: number, pairRow: Element) => {
                if (this.isInteresting(pairRow)) {
                    interestingMatchesUrls.push(this.getMatchUrl(pairRow));
                }
            });
            return interestingMatchesUrls;
        }

        private isInteresting(pairRow: Element) {
            return this.hasInterestingCoeficient(pairRow)
                && this.hasInterestingSpecifics(pairRow);
        }

        private getMatchUrl(pairRow: Element) {
            var rowLiveChildren = $(pairRow).children();
            var matchUrlContainer = rowLiveChildren.filter(function () {
                return $(this).text().trim().indexOf(" v ") > -1;
            });
            var matchUrl = matchUrlContainer.children().attr("href");
            return matchUrl;
        }

        private hasInterestingCoeficient(pairRow: Element) {
            var coefficientContainers =
                $(pairRow).find(".eventpriceholder-left").find(".eventprice");
            for (var i = 0; i < coefficientContainers.length; ++i) {
                var fraction = coefficientContainers.get(i).innerText.trim().split("/");
                var coefficient = parseInt(fraction[0]) / parseInt(fraction[1]);
                if (0.001 <= coefficient && coefficient <= 0.01) {
                    return true;
                }
            }
            return false;
        }

        //override this
        protected hasInterestingSpecifics(pairRow: Element): boolean {
            throw new Error('OfferAnalyzer.isInteresting method is abstract');
        }
    }
}