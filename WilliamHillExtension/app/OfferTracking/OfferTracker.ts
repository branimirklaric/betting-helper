﻿namespace OfferTracking {
    export class OfferTracker implements OfferTrackerObserver {
        private trackOffersCallback = this.trackOffers.bind(this);

		constructor(private offerChecker: OfferChecker) {
            this.offerChecker.addObserver(this);
		}

		trackOffers() {
            try {
				this.offerChecker.checkOffer();
            } catch (something) {
                if (something.localeCompare("reload") === 0) {
					chrome.runtime.sendMessage({ reloadOffers: true });
				}
            }
            setTimeout(this.trackOffersCallback, 30000);
		}

		recieveMatchUrls(matchUrls: string[]) {
			matchUrls.forEach((matchUrl: string) => {
				chrome.runtime.sendMessage({ matchUrl: matchUrl });
			});
		}
    }
}

