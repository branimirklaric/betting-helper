﻿namespace OfferTracking {
	export class OfferAnalyzerFactory {
		createOfferAnalyzer(sportName: string): OfferAnalyzer {
			var offerTrackingModule = window["OfferTracking"];
			var offerAnalyzerClassName = sportName + "OfferAnalyzer";
			var offerAnalyzer: OfferAnalyzer =
				Object.create(offerTrackingModule[offerAnalyzerClassName].prototype);
			//offerAnalyzer.constructor.call(offerAnalyzer);
			return offerAnalyzer;
		}
	}
}