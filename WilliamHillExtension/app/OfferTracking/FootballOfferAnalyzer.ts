﻿namespace OfferTracking {
	export class FootballOfferAnalyzer extends OfferAnalyzer {
		protected hasInterestingSpecifics(pairRow: Element) {
			var matchMinute = parseInt($(pairRow).children()[0].innerText.split(":")[0]);
			return matchMinute > 75;
		}
	}
}