﻿class EnumEx {
    static getNames(enumType: any): Array<string> {
        var enumNames: Array<string> = [];
        for (var val in enumType) {
            if (isNaN(val)) {
                enumNames.push(val);
            }
        }
        return enumNames;
    }

    static getValues(enumType: any): Array<number> {
        var enumValues: Array<number> = [];
        for (var val in enumType) {
            if (!isNaN(val)) {
                enumValues.push(parseInt(val, 10));
            }
        }
        return enumValues;
    }
}