﻿class MatchTabMarker {
    //puts an asterisk at the beggining of page title to indicate the page hasn't been read
    static markTabAsUnread() {
        var tabTitle = $("title").text();
        $("title").text("* " + tabTitle);
    }
}

$(document).ready(MatchTabMarker.markTabAsUnread);