﻿class PriceConverter {
    static convertPrices() {
        var priceContainers = $(".eventprice, .eventpriceup, .eventpricedown");
        priceContainers.each((index, elem) => {
            if (elem.textContent.indexOf("/") > -1) {
                var fraction = elem.textContent.trim().split("/");
                var coefficient = parseInt(fraction[0]) / parseInt(fraction[1]);
                elem.textContent = (coefficient * 100).toFixed(2) + "%";
            }
        });
        setTimeout(PriceConverter.convertPrices, 1000);
    }
}

$(document).ready(() => setTimeout(PriceConverter.convertPrices, 1000));