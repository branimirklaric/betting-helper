﻿class BettingExpiredChecker {
    //periodically checks if betting has expired for a match and closes the tab if it has
	static checkIfBettingExpired() {
		var breadCrumbText = $("#breadcrumb").children().text();
		var bettingClosed = (breadCrumbText.localeCompare("Betting Closed") === 0)
			|| (breadCrumbText.localeCompare("Betting expired") === 0);
		var emptyPage = $("body").children().length < 10;
		if (bettingClosed || emptyPage) {
			chrome.runtime.sendMessage({ closeTab: true });
		} else {
			setTimeout(BettingExpiredChecker.checkIfBettingExpired, 20000);
		}
	}
}

$(document).ready(() => setTimeout(BettingExpiredChecker.checkIfBettingExpired, 1000));