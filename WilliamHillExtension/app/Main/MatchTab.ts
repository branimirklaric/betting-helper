﻿import R = Reminding;

class MatchTab {
    static main() {
        var reminderFormModelFactory = new R.ReminderFormModelFactory();
        var reminderFormModelGetter =
            new R.ReminderFormModelGetter(reminderFormModelFactory);
        chrome.runtime.onMessage.addListener(
            reminderFormModelGetter.getReminderFormModel.bind(reminderFormModelGetter));

        var titleChanger = new R.TitleChanger();
        var reminderTrackerFactory = new R.ReminderTrackerFactory(titleChanger);
        var reminderSetup = new R.ReminderSetup(reminderTrackerFactory);
        chrome.runtime.onMessage.addListener(
            reminderSetup.useReminderTracker.bind(reminderSetup));
    }
}

MatchTab.main();