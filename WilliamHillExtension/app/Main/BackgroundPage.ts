﻿import B = Background;

class BackgroundPage {
    static main() {
        var soundPlayer =
            new B.SoundPlayer("../sounds/notification.mp3", "../sounds/reminder.mp3");
        var reminderActivator = new B.ReminderActivator(soundPlayer);
        var matchTabCreator = new B.MatchTabCreator(soundPlayer);
        var offersTabReloader = new B.OffersTabReloader();
        var matchTabUnmarker = new B.MatchTabUnmarker();
        var tabCloser = new B.TabCloser();

        chrome.runtime.onMessage.addListener(
            reminderActivator.activateReminder.bind(reminderActivator));
        chrome.runtime.onMessage.addListener(
            matchTabCreator.createNewMatchTab.bind(matchTabCreator));
        chrome.runtime.onMessage.addListener(
            offersTabReloader.reloadOffersTab.bind(offersTabReloader));
        chrome.tabs.onActivated.addListener(
            matchTabUnmarker.unmarkAsUnread.bind(matchTabUnmarker));
        chrome.runtime.onMessage.addListener(
            tabCloser.closeTabListener.bind(tabCloser));

        chrome.runtime.onInstalled.addListener(B.ExtensionSetup.setUpExtension);

        var contextMenuSetup = new B.ContextMenuSetup(tabCloser);
        contextMenuSetup.setUpContextMenu();
    }
}

BackgroundPage.main();