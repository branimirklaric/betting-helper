﻿import OF = OfferTracking;

class OffersTab {
    static main() {
		var priceFormatChecker = new OF.PriceFormatChecker();
		var offerAnalyzerFactory = new OF.OfferAnalyzerFactory();
        var offerChecker = new OF.OfferChecker(priceFormatChecker, offerAnalyzerFactory);
        var offerTracker = new OF.OfferTracker(offerChecker);
		offerTracker.trackOffers();
    }
}

$(window).load(OffersTab.main);