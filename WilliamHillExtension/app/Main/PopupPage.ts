﻿import R = Reminding;

class PopupPage {
    static main() {
        var reminderFormSetupFactory = new R.ReminderFormSetupFactory();
        var poputSetup = new R.PopupSetup(reminderFormSetupFactory);
        poputSetup.setUpPopup();
    }
}

$(document).ready(PopupPage.main);