﻿namespace Background {
    export class TabCloser {
        //closes the senders tab
        closeTabListener(message, sender: chrome.runtime.MessageSender) {
            if (message.closeTab === undefined) {
                return;
            }
            chrome.tabs.remove(sender.tab.id);
        }

        closeTab(tabId: number) {
            chrome.tabs.remove(tabId);
        }
    }
}