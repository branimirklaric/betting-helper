﻿namespace Background {
    export class MatchTabCreator {
        constructor(private soundPlayer: SoundPlayer) {
        }

        //creates a new match tab with the received url and plays a notification sound
        createNewMatchTab(message, sender: chrome.runtime.MessageSender) {
            if (message.matchUrl === undefined) {
                return;
            }
            var matchUrl = decodeURI(message.matchUrl);

            var checkIfOnIgnoreList = new Promise<boolean>((resolve) => {
                this.onIgnoreList(resolve, matchUrl);
            });

            checkIfOnIgnoreList.then((value) => {
                this.doCreateNewMatchTab(matchUrl, sender.tab.windowId);
            });
        }

        private onIgnoreList(resolve, matchUrl: string) {
            chrome.storage.sync.get("urlIgnoreList", (items) => {
                var urlIgnoreList = <Array<UrlIgnoreInfo>>items["urlIgnoreList"];
                var filtered = urlIgnoreList.filter((urlIgnoreInfo) => {
                    var ret = urlIgnoreInfo.url.localeCompare(matchUrl) === 0;
                    return ret;
                });
                var onIgnoreList = filtered.length > 0;
                if (!onIgnoreList) {
                    resolve(true);
                }
            });
        }

        private doCreateNewMatchTab(matchUrl: string, windowWithOffersTab: number) {
            chrome.tabs.query({ url: matchUrl }, (tabs) => {
                if (tabs.length > 0) {
                    return;
                }
                var tabCreateProperties: chrome.tabs.CreateProperties = {
                    windowId: windowWithOffersTab,
                    url: matchUrl,
                    active: false
                };
                chrome.tabs.create(tabCreateProperties);
                this.soundPlayer.playNotificationSound();
            });
        }
    }
}