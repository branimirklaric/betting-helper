﻿namespace Background{
    export class ExtensionSetup {
        //sets up rules that show extension page action on offer and match tabs
        static setUpExtension() {
            chrome.declarativeContent.onPageChanged.removeRules(undefined, () => {
                var offersTabMatcher = new chrome.declarativeContent.PageStateMatcher({
                    pageUrl: {
                        urlEquals: 'http://sports.williamhill.com/bet/en-gb/betlive/all'
                    }
                });
                var matchTabMatcher = new chrome.declarativeContent.PageStateMatcher({
                    pageUrl: {
                        urlContains: 'http://sports.williamhill.com/bet/en-gb/betting/e/'
                    }
                });
                var showPageAction = new chrome.declarativeContent.ShowPageAction();
                var rule = {
                    conditions: [offersTabMatcher, matchTabMatcher],
                    actions: [showPageAction]
                };
                chrome.declarativeContent.onPageChanged.addRules([rule]);
            });
        }
    }
}