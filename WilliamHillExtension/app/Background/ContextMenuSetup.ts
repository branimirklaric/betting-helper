﻿namespace Background {
    export class ContextMenuSetup {
        constructor(private tabCloser: TabCloser) {
        }

        setUpContextMenu() {
            var contextMenuCreateProperties: chrome.contextMenus.CreateProperties = {
                id: "permaCloser",
                title: "Don't track this match"
            }
            chrome.contextMenus.create(contextMenuCreateProperties);
            chrome.contextMenus.onClicked.addListener(
                this.contextMenuOnClicked.bind(this));

            chrome.storage.sync.set({ urlIgnoreList: [] });
        }

        private contextMenuOnClicked(
            info: chrome.contextMenus.OnClickData, tab: chrome.tabs.Tab) {
            var pageUrl = decodeURI(info.pageUrl);
            var isMatchPage = pageUrl.indexOf(
                "http://sports.williamhill.com/bet/en-gb/betting/e/") > -1;
            if (isMatchPage) {
                this.stopTrackingUrl(pageUrl, tab.id);
            }
        }

        private stopTrackingUrl(urlToIgnore: string, tabId: number) {
            chrome.storage.sync.get("urlIgnoreList", (items: Object) => {
                var urlIgnoreList = <Array<UrlIgnoreInfo>>(items["urlIgnoreList"]);
                var currentHour = new Date().getHours();
                urlIgnoreList = urlIgnoreList.filter((urlIgnoreInfo) => {
                    return urlIgnoreInfo.hour > currentHour - 3;
                });
                urlIgnoreList.push(
                    new UrlIgnoreInfo(new Date().getHours(), urlToIgnore));
                chrome.storage.sync.set({ urlIgnoreList: urlIgnoreList }, () => {
                    this.tabCloser.closeTab(tabId);
                });
            });
        }
    }

    export class UrlIgnoreInfo {
        constructor(public hour: number, public url: string) {
        }
    }
}