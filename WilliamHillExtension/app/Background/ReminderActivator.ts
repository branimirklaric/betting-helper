﻿namespace Background {
    export class ReminderActivator {
        constructor(private soundPlayer: SoundPlayer) {
        }

        activateReminder(message, sender: chrome.runtime.MessageSender) {
            if (message.activateReminder === undefined) {
                return;
            }
            console.log("recieved a reminder");
            var highlightInfo: chrome.tabs.HighlightInfo = {
                windowId: sender.tab.windowId,
                tabs: [sender.tab.index]
            };
            chrome.tabs.highlight(highlightInfo);
            this.soundPlayer.playReminderSound();
        }
    }
}