﻿namespace Background {
    export class OffersTabReloader {
        //reloads the offers tab
        reloadOffersTab(message) {
            if (message.reloadOffers === undefined) {
                return;
            }

            var offersTabQueryInfo: chrome.tabs.QueryInfo = {
                url: "http://sports.williamhill.com/bet/en-gb/betlive/all"
            };
            chrome.tabs.query(offersTabQueryInfo, this.doReloadOffersTab);
        }

        private doReloadOffersTab(tabs: chrome.tabs.Tab[]) {
            chrome.tabs.update(tabs[0].id, {
                url: "http://sports.williamhill.com/bet/en-gb/betlive/all"
            });
        }
    }
}