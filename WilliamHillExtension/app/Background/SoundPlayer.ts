﻿namespace Background {
    export class SoundPlayer {
        private notificationSound: HTMLAudioElement;
        private reminderSound: HTMLAudioElement;

        constructor(notificationSoundPath: string, reminderSoundPath: string) {
            this.notificationSound = new Audio("../sounds/notification.mp3");
            this.reminderSound = new Audio("../sounds/reminder.mp3");
        }

        playNotificationSound() {
            this.notificationSound.play();
        }

        playReminderSound() {
            this.reminderSound.play();
        }
    }
}