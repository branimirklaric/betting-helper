﻿namespace Background {
    export class MatchTabUnmarker {
        //if the newly activated tab is marked as unread, removes the asterisk at the beggining of its title
        unmarkAsUnread(activeInfo: chrome.tabs.TabActiveInfo) {
            var offersTabQueryInfo: chrome.tabs.QueryInfo = {
                url: "http://sports.williamhill.com/bet/en-gb/betlive/all"
            };
            chrome.tabs.query(offersTabQueryInfo, (tabs) => {
                //the one and only result tab is the offers tab (unless several of them are open)
                if (tabs[0].windowId != activeInfo.windowId) {
                    return;
                }
                chrome.tabs.get(activeInfo.tabId, (tab) => {
                    if (tab.title.indexOf("* ") != 0) {
                        return;
                    }
                    chrome.tabs.executeScript(tab.id, {
                        code: 'var title = $("title").text(); $("title").text(title.substring(2));'
                    });
                });
            });
        }
    }
}